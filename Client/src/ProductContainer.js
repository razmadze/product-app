
import React from 'react';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import ProductList from './components/ProductList';
import ProductAdd from './components/ProductAdd';
import createBrowserHistory from 'history/createBrowserHistory';

const history = createBrowserHistory();

class ProductContainer extends React.Component {
    render() {
        return (
            <div className='parentContainer'>
                <Router history={history}>
                    <Switch>
                        <Route path='/productlist' render={() => <ProductList />} />
                        <Route path='/productadd' render={() => <ProductAdd />} />
                        <Route exact path='/' render={() => {
                            return <Redirect to='/productlist' />
                        }} />
                    </Switch>
                </Router>
            </div>
        );
    }
};

export default ProductContainer;