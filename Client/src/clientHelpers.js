import { store } from './components/ProductList';

//
// GetProductList sends response to store.
//

export const getProductList = () => {
  fetch('http://localhost/productlist/index.php?mode=request')
    .then(res => res.json())
    .then(
      (result) => {
        store.dispatch({
          type: 'GET_PRODUCTLIST',
          products: result
        });
      }
    );
};

//
// Adding one Product to database, and after okay updating store.
//

export const addOne = (product) => {
  const newProduct = Object.assign({}, product, {
    dimension: `${product.height}x${product.width}x${product.length}`
  });
  fetch('http://localhost/productlist/index.php?mode=addProduct', {
    method: 'POST',
    body: JSON.stringify({
      toAdd: newProduct
    })
  }).then(res => res.json()
  ).then(json => {
    if (json !== 'SKU' && json.status !== 'ERROR') {
      newProduct.id = json.id;
      store.dispatch({
        type: 'ADD_ONE',
        product: newProduct
      });
    }
  });
};

//
// Deleting Product from database, and after okay updating store.
//

export const deleteAction = (products, deleted) => {
  fetch('http://localhost/productlist/index.php?mode=deleteProduct', {
    method: 'POST',
    body: JSON.stringify({
      toDelete: deleted
    })
  }).then(res => {
    if (res.statusText === 'OK') {
      store.dispatch({
        type: 'DELETE_PRODUCT',
        products: products
      });
    } else {
      window.localStorage.reload();
    }
  }
  );
};
