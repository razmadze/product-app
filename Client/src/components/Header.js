import React from 'react';
import { Link } from 'react-router-dom';

//
// Header is responsible for routing, here is provided classic react-router-dom.
//
const Header = (props) => {
    return (
        <header className="header">
            <Link
                to='/productList'
            >
                <div className="header--logo"></div>
            </Link>

            <Link
                to='/productadd'
                className={props.isActive === 'productadd' ? 'header--route active' : 'header--route'}
            >
                ProductAdd
                    </Link>
            <Link
                to='/productList'
                className={props.isActive === 'productlist' ? 'header--route active' : 'header--route'}
            >
                ProductList
                    </Link>
        </header>
    );
};

export default Header;