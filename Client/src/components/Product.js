import React from 'react';

// Product Component is responsible for rendering itself and also
// for handling Click on it and sending to parent with function which, is provided,
// by props.

class Product extends React.Component {
  onClickHandler = () => {
    this.props.productClick(this.props.id);
  }
  render() {
    const property = (props) => {
      switch (props.type) {
        case 'book':
          return <p>
            {props.weight / 1000} KG
                 </p>;
        case 'disc':
          return <p>
            {props.size} MB
                 </p>;
        case 'furniture':
          return <p>
            {props.dimension}
          </p>;
        default:
          return;
      }
    };
    return (
      <div
        id={this.props.id}
        key={this.props.id}
        className={this.props.isActive ? 'item red' : 'item'}
        onClick={this.onClickHandler}
      >
        <p>
          {this.props.sku}
        </p>
        <p>
          {this.props.name}
        </p>
        <p>
          {this.props.price} $
        </p>
        {property(this.props)}
      </div>
    );
  }
}

export default Product;