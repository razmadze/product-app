import React from 'react';
import { store } from './ProductList';
import { Redirect } from 'react-router-dom';
import Header from './Header';
import { addOne } from '../clientHelpers';

// ProductAdd, has single field state, which updates on every form change.
// Also it has errors array state, checking if user provided correct information.
// it is using same store, created in productList, to update with new product.

class ProductAdd extends React.Component {
  state = {
    fields: {
      sku: '',
      name: '',
      price: '',
      type: '',
      weight: '',
      size: '',
      height: '',
      width: '',
      length: ''
    },
    errors: [{

    }],
    redirect: false
  };

  onFormSubmit = (evt) => {
    let shouldPass = this.validate();
    shouldPass = (Object.keys(shouldPass[0]).length);
    if (shouldPass === 0) {
      addOne(this.state.fields);
      this.setState({
        fields: {
          sku: '',
          name: '',
          price: '',
          type: '',
          weight: '',
          size: '',
        },
        redirect: true
      });
    } else {
      this.setState({
        errors: this.validate()
      });
    }

    evt.preventDefault();
  }

  onInputChange = (evt) => {
    const fields = this.state.fields;
    if (evt.target.name === 'price' ||
      evt.target.name === 'width' ||
      evt.target.name === 'height' ||
      evt.target.name === 'length' ||
      evt.target.name === 'size' ||
      evt.target.name === 'weight') {

      if (this.checkNumber(evt.target.value)) return;
    };

    fields[evt.target.name] = evt.target.value;
    this.setState({ fields })
  };

  handleSelect = (evt) => {
    if (evt.target.value !== 'type switcher') {
      const fields = this.state.fields;
      fields['type'] = evt.target.value;
      this.setState({ fields });
    } else {
      const fields = this.state.fields;
      fields['type'] = '';
      this.setState({ fields });
    }
  };

  checkSku = (SKU) => {
    return store.getState().products.filter(product => product.sku === SKU);
  };

  checkNumber = (value) => {
    return isNaN(value);
  };

  validate = () => {
    let errorsArr = [{}];

    if (!this.state.fields.name) {
      errorsArr[0].name = 'Please provide product name.';
    };

    if (!this.state.fields.price || this.checkNumber(this.state.fields.price)) {
      errorsArr[0].price = 'Please provide product price.';
    };

    if (!this.state.fields.sku || !!this.checkSku(this.state.fields.sku).length) {
      errorsArr[0].sku = 'Please provide unique product sku.';
    };

    if (!this.state.fields.type) {
      errorsArr[0].type = 'Please provide product type.';
    }

    if (this.state.fields.type === 'book' && !this.state.fields.weight) {
      errorsArr[0].weight = 'Please provide book Weight.';
    }

    if (this.state.fields.type === 'disc' && !this.state.fields.size) {
      errorsArr[0].size = 'Please provide disc size.';
    }

    if (this.state.fields.type === 'furniture' &&
      (!this.state.fields.height || !this.state.fields.width || !this.state.fields.length)) {
      errorsArr[0].dimension = 'Please provide furniture dimension.';
    }
    return errorsArr;
  };

  render() {
    if (this.state.redirect) return <Redirect push to="/productlist" />;
    return (
      <div>
        <Header isActive="productadd" />
        <div className='header--info'>
          <h1>Product Add</h1>

          <div className='header--info-action'>
            <input type='submit' value="Save" onClick={this.onFormSubmit} />
          </div>
          <div style={{ clear: 'both' }}></div>
        </div>
        <form onSubmit={this.onFormSubmit}>
          <ul>
            <li>
              <span className="addProductInfoTitle">
                SKU
            </span>
              <input
                placeholder='SKU'
                name='sku'
                value={this.state.fields.sku}
                onChange={this.onInputChange}
              />
              <span className='danger'>{this.state.errors[0].sku}</span>
            </li>
            <li>
              <span className="addProductInfoTitle">
                Name
              </span>
              <input
                placeholder='Name'
                name='name'
                value={this.state.fields.name}
                onChange={this.onInputChange}
              />
              <span className='danger'>{this.state.errors[0].name}</span>
            </li>
            <li>
              <span className="addProductInfoTitle">
                Prise
              </span>
              <input
                placeholder='Price'
                name='price'
                value={this.state.fields.price}
                onChange={this.onInputChange}
              />
              <span className='danger'>{this.state.errors[0].price}</span>
            </li>
            <li>
              <span className="addProductInfoTitle">
                Type switcher
              </span>
              <select onChange={this.handleSelect}>
                <option value='type switcher'>
                  Type switcher
                </option>
                <option value='book'>
                  Book
              </option>
                <option value='furniture'>
                  Furniture
              </option>
                <option value='disc'>
                  Disc
              </option>
              </select>
              <span className='danger'>{this.state.errors[0].dimension}</span>
              <span className='danger'>{this.state.errors[0].type}</span>
            </li>
            <li className={this.state.fields.type !== 'book' ? 'typeDisabled' : ''}>
              <span className="addProductInfoTitle">
                Weight
              </span>
              <input
                placeholder='Weight'
                name='weight'
                value={this.state.fields.weight}
                onChange={this.onInputChange}
              />
              <span className='danger'>{this.state.errors[0].weight}</span>
              <div style={{ clear: 'both' }}></div>
              <span className="provideDescription">Please provide book weight in grams.</span>
            </li>
            <li className={this.state.fields.type !== 'disc' ? 'typeDisabled' : ''}>
              <span className="addProductInfoTitle">
                Size
              </span>
              <input
                placeholder='Size'
                name='size'
                value={this.state.fields.size}
                onChange={this.onInputChange}
              />
              <span className='danger'>{this.state.errors[0].size}</span>
              <div style={{ clear: 'both' }}></div>
              <span className="provideDescription">Please provide disc size in MB.</span>
            </li>
            <li className={this.state.fields.type !== 'furniture' ? 'typeDisabled' : ''}>
              <span className="addProductInfoTitle">
                Height
              </span>
              <input
                placeholder='Height'
                name='height'
                value={this.state.fields.height}
                onChange={this.onInputChange}
              />
            </li>
            <li className={this.state.fields.type !== 'furniture' ? 'typeDisabled' : ''}>
              <span className="addProductInfoTitle">
                Width
              </span>
              <input
                placeholder='Width'
                name='width'
                value={this.state.fields.width}
                onChange={this.onInputChange}
              />
            </li>
            <li className={this.state.fields.type !== 'furniture' ? 'typeDisabled' : ''}>
              <span className="addProductInfoTitle">
                Length
              </span>
              <input
                placeholder='Length'
                name='length'
                value={this.state.fields.length}
                onChange={this.onInputChange}
              />
              <div style={{ clear: 'both' }}></div>
              <span className="provideDescription">Please provide dimensions in HxWxL format.</span>
            </li>
          </ul>
        </form>
      </div>
    )
  }
};

export default ProductAdd;