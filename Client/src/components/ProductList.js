import React from 'react';
import Product from './Product';
import Header from './Header';
import { createStore, reducer } from './Store';
import { getProductList, deleteAction } from '../clientHelpers';

const initialState = { products: [] };

//For this project, I am using flux structure, It's factory function
//With local state, it's similar to redux, but minified.
//For mor info please check Store.js located in this folder.

export const store = createStore(reducer, initialState);
getProductList();

class ProductList extends React.Component {

  //ComponentDidMount is subscribing, if state changes in store.
  componentDidMount() {
    store.subscribe(() => this.forceUpdate());
  };

  //Sending Delete action to store, with updated products list and array of id's for PHP. 
  handleDeleteAction = () => {
    const products = store.getState().products.filter(product => !product.isActive);
    const toDelete = store.getState().products.filter(product => product.isActive)
    const deleted = toDelete.map((product) => product.id);

    deleteAction(products, deleted);
  };

  //Checking Product neither is active or not(Ready for delete). 

  handleProductClick = (index) => {
    store.dispatch({
      type: 'CHANGE_ACTION',
      index: index,
    });
  };

  render() {
    const products = store.getState().products.map((product, i) => <Product
      id={product.id}
      key={product.id}
      sku={product.sku}
      name={product.name}
      size={product.size}
      price={product.price}
      type={product.type}
      weight={product.weight}
      dimension={product.dimension}
      isActive={product.isActive}
      productClick={this.handleProductClick}
    />
    );
    return (
      <div>
        <Header isActive='productlist' />
        <div className='header--info'>
          <h1>Product List</h1>
          <button className="applyButton" onClick={this.handleDeleteAction}>
            Apply
          </button>
          <div className='header--info-action'>
            <select>
              <option value='Mass Delete Action'>
                Mass Delete Action
              </option>
            </select>
          </div>
          <div style={{ clear: 'both' }}></div>
        </div>
        <div>
          {products}
        </div>
      </div>
    );
  }
};

export default ProductList;