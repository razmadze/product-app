export const createStore = (reducer, initialState) => {
    let state = initialState;
    const listeners = [];

    const subscribe = (listener) => (
        listeners.push(listener)
    );

    const getState = () => (state);

    const dispatch = (action) => {
        state = reducer(state, action);
        listeners.forEach(l => l());
    };

    return {
        subscribe,
        getState,
        dispatch,
    };
};

export const reducer = (state, action) => {
    if (action.type === 'GET_PRODUCTLIST') {
        return {
            products: action.products,
        };
    } else if (action.type === 'DELETE_PRODUCT') {
        return {
            products: action.products
        };
    } else if (action.type === 'CHANGE_ACTION') {
        return {
            products: state.products.map((product) => {
                if (product.id === action.index) {
                    return Object.assign({}, product, {
                        isActive: product.isActive ? false : true,
                    })
                } else {
                    return product;
                }
            })
        };
    } else if (action.type === 'ADD_ONE') {
        state.products.concat(action.product)
        return {
            products: [action.product, ...state.products]
        }
    } else {
        return state;
    }
};