import React from 'react';
import ReactDOM from 'react-dom';
import ProductContainer from './ProductContainer'
import './styles/App.scss';
import * as serviceWorker from './serviceWorker';



ReactDOM.render(<ProductContainer />, document.getElementById('root'));


serviceWorker.unregister();