**Installation:**

1. Move index.php file from '/Server' to 'path/to/apache-server/productlist'
2. Create dataBase called 'productlist' in e.g phpMyAdmin
3. Import 'productlist.sql' file into 'productlist' database
4. cd Client
5. run npm install
6. run npm start
7. Default port is 3000