<?php
    namespace site;

    class App
    {
        private $model;
        
        public function __construct()
        {
            $this->model = new ProductListModel(SERVER_NAME, USER_NAME, PASSWORD, DB_NAME);
        }

        //Checks REQUEST data validation and if everything is okay it runs 
        //concretic function but if there is any kind of error it returns 'ERROR'
        public function start()
        {
            if(isset($_REQUEST['mode'])) 
            {
                if ($_REQUEST['mode'] == 'request') 
                {
                    echo $this->model->getInfo();
                } 
                else if($_REQUEST['mode'] == 'addProduct') 
                {
                    $requestInfo = file_get_contents('php://input');

                    $data = json_decode($requestInfo, true);

                    if(!isset($data['toAdd']) || !is_array($data['toAdd']) )
                    {
                        die('ERROR');
                    }

                    $data = $data['toAdd'];

                    if(!isset($data['sku']) || trim($data['sku']) == '') 
                    {
                        die('ERROR');
                    }

                    if(!isset($data['name']) || trim($data['name']) == '') 
                    {
                        die('ERROR');
                    }

                    if(!isset($data['price']) || !is_numeric($data['price'])) 
                    {
                        die('ERROR');
                    }

                    if(!isset($data['type']) || trim($data['type']) == '') 
                    {
                        die('ERROR');
                    }

                    $type = $data['type'];

                    switch ($type) 
                    {
                        case 'book':
                            if(!isset($data['weight']) || !is_numeric($data['weight'])) 
                            {
                                die('ERROR');
                            }
                            break;
                        case 'furniture':
                            if(!isset($data['dimension']) || trim($data['dimension']) == '') 
                            {
                                die('ERROR');
                            }
                            break;
                        case 'disc':
                            if(!isset($data['size']) || !is_numeric($data['size'])) 
                            {
                                die('ERROR');
                            }
                            break;
                        default:
                            break;
                    }



                    if (sizeof($this->model->getOneProduct($data['sku'])) == 0) 
                    {
                        echo $this->model->addData($data);
                    }
                    else 
                    {
                        echo 'SKU';
                    }
                } 
                else if($_REQUEST['mode'] == 'deleteProduct') 
                {
                    $requestInfo = file_get_contents('php://input');

                    $data = json_decode($requestInfo, true);

                    if(!isset($data['toDelete']) || !is_array($data['toDelete'])) 
                    {
                        die('ERROR');
                    }
        
                    echo $this->model->deleteData($data);
                }
            }
        }
    }
?>