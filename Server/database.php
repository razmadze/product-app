<?php
	namespace site;
	use \PDO;

	class DataBase
	{
		public $conn = null;

		//Configures MySQL base connection properties
		public function __construct($serverName, $userName, $password, $dbName)
		{
			$this->conn = new PDO("mysql:host=$serverName;dbname=$dbName", $userName, $password);
			$this->conn->exec('set names utf8');
			$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
	}
?>