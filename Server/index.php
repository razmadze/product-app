<?php
header("Access-Control-Allow-Origin: *");

require_once('config.php');
require_once('database.php');
require_once('app.php');
require_once('productlistmodel.php');

$App = new \site\App();
$App->start();
?>