<?php
    namespace site;

    class ProductListModel
    {
        private $db;

        public function __construct($serverName, $userName, $password, $dbName)
        {
            $this->db = new DataBase($serverName, $userName, $password, $dbName);
        }

		//Gets products list from MySQL base and returns json data to react
        public function getInfo()
        {
            header('Content-type:application/json');

			$result = $this->db->conn->prepare('SELECT * FROM information ORDER BY _id DESC');
			$result->execute();
			$result = $result->fetchAll();
			$returnInfo = [];

			foreach ($result as $key => $value) 
			{
				$returnInfo[] = array(
					'id'        => $value['_id'],
					'sku'       => $value['_sku'],
					'name'      => $value['_name'],
					'price'     => $value['_price'],
					'type'      => $value['_type'],
					'size'      => $value['_size'],
					'dimension' => $value['_dimension'],
					'weight'    => $value['_weight']
				);
			}

			return json_encode($returnInfo);
        }

		//Gets product from MySQL base where '_sku' equals $sku and returns array
        public function getOneProduct($sku) 
        {
        	$values = [];
        	$result = $this->db->conn->prepare('SELECT * FROM information WHERE _sku = ? ORDER BY _id DESC');

        	$values[] = $sku;

			$result->execute($values);
			$result = $result->fetchAll();

			return $result;

        }
        
		//Deletes products information into MySQL base and returns 'OK' and product 'id'
		//if request was successful, or returns 'ERROR' is there was some kind of problem.        
        public function addData($data = array()) 
        {
        	header('Content-type:application/json');

            $values = [];
			$result = $this->db->conn->prepare('INSERT INTO ' . 
				'information (_sku, _type, _price, _name, _weight, _size, _dimension)'.
				' VALUES (?, ?, ?, ?, ?, ?, ?)');

			$values[] = $data['sku'];
			$values[] = $data['type'];
			$values[] = $data['price'];
			$values[] = $data['name'];
			$values[] = $data['weight'];
			$values[] = $data['size'];
			$values[] = $data['dimension'];

			$jsonInfo = [];

			try 
			{
				$result->execute($values);
				$oneProduct = $this->getOneProduct($data['sku']);
				if (sizeof($this->getOneProduct($data['sku'])) == 1) 
				{
					$jsonInfo['id']		 = $oneProduct[0]['_id'];
					$jsonInfo['status']  = 'OK';
				}
			}
			catch(Exception $error) 
			{
				$jsonInfo['status'] = 'ERROR';
			}

			return json_encode($jsonInfo);
        }

		//Adds product information into MySQL base and returns 'OK' if request was successful,
		//or returns 'ERROR' is there was some kind of problem.
        public function deleteData($data) 
        {
			$mainResult = 'OK';
			foreach ($data['toDelete'] as $key => $value) 
			{
				$values = [];
				$result = $this->db->conn->prepare('DELETE FROM `information` WHERE _id = ?');
				$values[] = $value;

				try 
				{
					$result->execute($values);
				}
				catch(Exception $error) 
				{
					$mainResult = 'ERROR';
				}
			}

			return $mainResult;
		}
    }
?>